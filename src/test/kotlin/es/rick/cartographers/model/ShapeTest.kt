package es.rick.cartographers.model

import es.rick.cartographers.model.map.ImmutableShape
import es.rick.cartographers.model.map.Shape
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ShapeTest {
    @Test
    fun squareVariations() {
        val shape = Array(2) { BooleanArray(2) { true } }
        val square = ImmutableShape.builder().shape(*shape).build()
        val variations = square.variations
        printShapes(variations)
        Assertions.assertEquals(1, variations.size)
    }

    @Test
    fun lineVariations() {
        val shape = Array(1) { BooleanArray(4) { true } }
        val line = ImmutableShape.builder().shape(*shape).build()
        val variations = line.variations
        printShapes(variations)
        Assertions.assertEquals(2, variations.size)
    }

    @Test
    fun sVariations() {
        val shape = Array(2) { BooleanArray(3) { true } }
        shape[0][0] = false
        shape[1][2] = false
        val s = ImmutableShape.builder().shape(*shape).build()
        val variations = s.variations
        printShapes(variations)
        Assertions.assertEquals(4, variations.size)
    }

    @Test
    fun s2Variations() {
        val shape = Array(2) { BooleanArray(4) { true } }
        shape[0][0] = false
        shape[1][2] = false
        shape[1][3] = false
        val s = ImmutableShape.builder().shape(*shape).build()
        val variations = s.variations
        printShapes(variations)
        Assertions.assertEquals(8, variations.size)
    }

    private fun printShapes(shapes: Array<Shape>) {
        println("Shapes")
        shapes.forEach { println(it.toString() + System.lineSeparator()) }
    }
}