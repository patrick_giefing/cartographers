package es.rick.cartographers.service;

import es.rick.cartographers.model.game.Game;
import es.rick.cartographers.model.map.Map;
import es.rick.cartographers.model.player.Player;
import es.rick.cartographers.spring.launcher.CartographersLauncher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = CartographersLauncher.class)
public class GameServiceTest {
    @Autowired
    private GameService gameService;

    @Test
    public void startGame() {
        List<Player> players = Player.createRandomPlayers(5);
        Map map = Map.createRandomMap(11, 11, 5, 5);
        Game game = gameService.startGame(players, map);
    }
}
