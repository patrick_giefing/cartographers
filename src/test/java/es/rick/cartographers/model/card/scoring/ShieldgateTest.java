package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Forest;

public class ShieldgateTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Village, Village, Village, Village},
                {Forest, Village, Forest, Village},
                {Forest, Empty, Village, Forest},
                {Village, Village, Village, Forest}
        });
        Shieldgate shieldgate = new Shieldgate();
        int points = shieldgate.calculatePoints(map);
        Assertions.assertEquals(8, points);
    }
}
