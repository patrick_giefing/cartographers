package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Forest;

public class MagesValleyTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Mountain, Water, Farm, Empty},
                {Forest, Water, Water, Farm},
                {Forest, Farm, Mountain, Forest},
                {Forest, Empty, Forest, Forest}
        });
        MagesValley magesValley = new MagesValley();
        int points = magesValley.calculatePoints(map);
        Assertions.assertEquals(5, points);
    }
}
