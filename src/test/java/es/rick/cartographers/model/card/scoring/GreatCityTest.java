package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;

public class GreatCityTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Village, Village, Mountain, Village, Village},
                {Village, Village, Forest, Forest, Forest},
                {Water, Water, Forest, Water, Water},
                {Village, Village, Forest, Water, Water},
                {Village, Forest, Village, Village, Forest}
        });
        GreatCity greatCity = new GreatCity();
        int points = greatCity.calculatePoints(map);
        Assertions.assertEquals(3, points);
    }
}
