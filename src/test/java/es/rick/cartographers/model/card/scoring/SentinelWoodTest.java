package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import static es.rick.cartographers.model.map.Field.*;

public class SentinelWoodTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Empty, Water, Monster, Empty},
                {Forest, Water, Water, Farm},
                {Forest, Empty, Empty, Forest},
                {Forest, Empty, Forest, Forest}
        });
        SentinelWood sentinelWood = new SentinelWood();
        int points = sentinelWood.calculatePoints(map);
        Assertions.assertEquals(6, points);
    }
}
