package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Forest;

public class WildholdsTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Village, Village, Village, Village},
                {Village, Forest, Forest, Farm},
                {Forest, Village, Village, Village},
                {Forest, Village, Village, Village}
        });
        Wildholds wildholds = new Wildholds();
        int points = wildholds.calculatePoints(map);
        Assertions.assertEquals(8, points);
    }
}
