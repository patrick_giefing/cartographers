package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Forest;

public class ShoresideExpanseTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Farm, Empty, Water, Empty, Ruin},
                {Farm, Empty, Empty, Empty, Empty},
                {Forest, Farm, Farm, Forest, Empty},
                {Forest, Water, Village, Farm, Village},
                {Village, Village, Empty, Village, Village}
        });
        ShoresideExpanse shoresideExpanse = new ShoresideExpanse();
        int points = shoresideExpanse.calculatePoints(map);
        Assertions.assertEquals(3, points);
    }
}
