package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Village;

public class GreengoldPlainsTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Village, Water, Village, Village},
                {Village, Forest, Forest, Farm},
                {Farm, Forest, Village, Village},
                {Forest, Forest, Village, Village}
        });
        GreengoldPlains greengoldPlains = new GreengoldPlains();
        int points = greengoldPlains.calculatePoints(map);
        Assertions.assertEquals(6, points);
    }
}
