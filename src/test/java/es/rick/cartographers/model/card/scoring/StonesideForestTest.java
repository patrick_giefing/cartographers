package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;

public class StonesideForestTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Mountain, Water, Mountain, Empty},
                {Forest, Forest, Forest, Farm},
                {Forest, Farm, Mountain, Forest},
                {Forest, Mountain, Forest, Forest}
        });
        StonesideForest stonesideForest = new StonesideForest();
        int points = stonesideForest.calculatePoints(map);
        Assertions.assertEquals(21, points);
    }
}
