package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Forest;

public class CauldronsTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Water, Water, Monster, Empty},
                {Forest, Water, Forest, Farm},
                {Forest, Forest, Empty, Forest},
                {Forest, Empty, Forest, Forest}
        });
        Cauldrons cauldrons = new Cauldrons();
        int points = cauldrons.calculatePoints(map);
        Assertions.assertEquals(3, points);
    }
}
