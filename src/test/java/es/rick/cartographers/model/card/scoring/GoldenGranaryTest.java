package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.ImmutableShape;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Forest;

public class GoldenGranaryTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Ruin, Water, Farm, Empty},
                {Forest, Water, Water, Farm},
                {Forest, Farm, Ruin, Forest},
                {Forest, Empty, Forest, Forest}
        });
        map.setShapeInPosition(2, 2, ImmutableShape.builder().shape(new boolean[][]{{true}}).build(), Farm);
        GoldenGranary goldenGranary = new GoldenGranary();
        int points = goldenGranary.calculatePoints(map);
        Assertions.assertEquals(5, points);
    }
}
