package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.ImmutableShape;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;

public class BorderlandsTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Ruin, Water, Farm, Empty},
                {Forest, Water, Water, Farm},
                {Forest, Farm, Ruin, Forest},
                {Forest, Empty, Forest, Forest}
        });
        Borderlands borderlands = new Borderlands();
        int points = borderlands.calculatePoints(map);
        Assertions.assertEquals(6, points);
    }
}
