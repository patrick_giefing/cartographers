package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static es.rick.cartographers.model.map.Field.*;
import static es.rick.cartographers.model.map.Field.Forest;

public class CanalLakeTest {
    @Test
    public void calculatePointsAllEdges() {
        Map map = Map.create(new Field[][]{
                {Empty, Water, Farm, Empty},
                {Forest, Water, Water, Farm},
                {Forest, Farm, Empty, Forest},
                {Forest, Empty, Forest, Forest}
        });
        CanalLake canalLake = new CanalLake();
        int points = canalLake.calculatePoints(map);
        Assertions.assertEquals(6, points);
    }
}
