package es.rick.cartographers.model.map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static es.rick.cartographers.model.map.Field.*;

public class MapTest {
    @Test
    public void findAllFieldClusters() {
        Map map = Map.create(new Field[][]{
                {Forest, Forest, Farm, Empty},
                {Forest, Water, Water, Farm},
                {Forest, Water, Empty, Forest},
                {Forest, Empty, Forest, Forest}
        });
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(null);
        Assertions.assertEquals(5, fieldClusters.size());
        int fieldSum = fieldClusters.stream().map(fieldCluster -> fieldCluster.positions().size()).reduce((a, b) -> a + b).orElse(0);
        Assertions.assertEquals(13, fieldSum);
    }

    @Test
    public void findForestFieldClusters() {
        Map map = Map.create(new Field[][]{
                {Forest, Forest, Farm, Empty},
                {Forest, Water, Water, Farm},
                {Forest, Water, Empty, Forest},
                {Forest, Empty, Forest, Forest}
        });
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(Forest);
        Assertions.assertEquals(2, fieldClusters.size());
        int fieldSum = fieldClusters.stream().map(fieldCluster -> fieldCluster.positions().size()).reduce((a, b) -> a + b).orElse(0);
        Assertions.assertEquals(8, fieldSum);
    }
}
