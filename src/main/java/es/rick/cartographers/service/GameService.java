package es.rick.cartographers.service;

import es.rick.cartographers.model.card.ExploreCardDrawer;
import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.card.SeasonScoringCard;
import es.rick.cartographers.model.game.Game;
import es.rick.cartographers.model.game.ImmutableGame;
import es.rick.cartographers.model.game.ImmutableGameSheet;
import es.rick.cartographers.model.map.Map;
import es.rick.cartographers.model.player.Player;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("game")
public class GameService {
    public Game startGame(List<Player> players, Map map) {
        List<SeasonScoringCard> seasonScoringCards = ScoringCard.buildRandomSeasonScoringCards();
        ExploreCardDrawer exploreCardDrawer = ExploreCardDrawer.createRandomExploreCardDrawer();
        List<ImmutableGameSheet> gameSheets = players.stream().map(player -> ImmutableGameSheet.builder().player(player).map(map).build()).collect(Collectors.toList());
        Game game = ImmutableGame.builder().gameSheets(gameSheets).exploreCardDrawer(exploreCardDrawer).seasonScoringCards(seasonScoringCards).build();
        return game;
    }

    @GetMapping("start/random")
    public Game startRandomGame(@RequestParam("players") int playerCount) {
        List<Player> players = Player.createRandomPlayers(playerCount);
        Map map = Map.createRandomMap(11, 11, 4, 4);
        Game game = startGame(players, map);
        return game;
    }
}
