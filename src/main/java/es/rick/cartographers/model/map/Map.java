package es.rick.cartographers.model.map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.immutables.value.Value;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.fill;

public class Map {
    private final Field[][] fields;
    private final int[][] ruinsPositions;
    private final int[][] mountainsPositions;

    private Map(Field[][] fields) {
        this.fields = copy(fields);
        this.ruinsPositions = findRuinsPositions(fields);
        this.mountainsPositions = findMountainPositions(fields);
    }

    public int[][] getRuinsPositions() {
        return ruinsPositions;
    }

    @JsonIgnore
    public int[][] getMountainsPositions() {
        return mountainsPositions;
    }

    private static int[][] findRuinsPositions(Field[][] fields) {
        return findPositionsByField(fields, Field.Ruin);
    }

    private static int[][] findMountainPositions(Field[][] fields) {
        return findPositionsByField(fields, Field.Mountain);
    }

    public void setShapeInPosition(int row, int col, Shape shape, Field field) {
        boolean[][] shapeFields = shape.shape();
        for (int y = 0; y < shapeFields.length && row + y < fields.length; y++) {
            for (int x = 0; x < shapeFields[0].length && col + x < fields[0].length; x++) {
                if (shapeFields[y][x]) {
                    fields[row + y][col + x] = field;
                }
            }
        }
    }

    private static int[][] findPositionsByField(Field[][] fields, Field field) {
        List<int[]> ruinsPositions = new ArrayList<>();
        for (int row = 0; row < fields.length; row++) {
            for (int col = 0; col < fields[0].length; col++) {
                if (fields[row][col] == field)
                    ruinsPositions.add(new int[]{row, col});
            }
        }
        return ruinsPositions.toArray(new int[0][]);
    }

    @JsonProperty("fields")
    public Field[][] fields() {
        return copy(fields);
    }

    private static Field[][] copy(Field[][] fields) {
        Field[][] fieldsCloned = fields.clone();
        for (int i = 0; i < fieldsCloned.length; i++) {
            fieldsCloned[i] = fieldsCloned[i].clone();
        }
        return fieldsCloned;
    }

    public static Map create(Field[][] fields) {
        return new Map(fields);
    }

    public static Map createRandomMap(int rows, int cols, int ruins, int mountains) {
        Field[][] fields = new Field[rows][cols];
        for (Field[] rowFields : fields)
            fill(rowFields, Field.Empty);
        while (true) {
            int col = (int) (Math.random() * 10000) % cols;
            int row = (int) (Math.random() * 10000) % rows;
            if (fields[row][col] != Field.Empty)
                continue;
            if (ruins > 0) {
                fields[row][col] = Field.Ruin;
                ruins--;
                continue;
            }
            if (mountains > 0) {
                fields[row][col] = Field.Mountain;
                mountains--;
                continue;
            }
            break;
        }
        return new Map(fields);
    }

    public int cols() {
        return fields[0].length;
    }

    public int rows() {
        return fields.length;
    }

    private void checkCoordinates(int row, int col) {
        if (row < 0 || row >= fields.length || col < 0 || col >= fields[0].length)
            throw new IllegalArgumentException("Coordinates outside the map");
    }

    public List<int[]> getSurroundingFieldsPositions(int row, int col) {
        checkCoordinates(row, col);
        List<int[]> fieldsSurroundingPositions = new ArrayList<>();
        if (col > 0)
            fieldsSurroundingPositions.add(new int[]{row, col - 1});
        if (row > 0)
            fieldsSurroundingPositions.add(new int[]{row - 1, col});
        if (col < cols() - 1)
            fieldsSurroundingPositions.add(new int[]{row, col + 1});
        if (row < rows() - 1)
            fieldsSurroundingPositions.add(new int[]{row + 1, col});
        return fieldsSurroundingPositions;
    }

    public Field[] getSurroundingFields(int row, int col) {
        List<int[]> positions = getSurroundingFieldsPositions(row, col);
        List<Field> fields = positions.stream().map(position -> this.fields[position[0]][position[1]]).collect(Collectors.toList());
        return fields.toArray(new Field[0]);
    }

    public java.util.Map<Field, Long> getSurroundingFieldCounts(FieldCluster fieldCluster) {
        Set<Position> allSurroundingFieldsPositions = fieldCluster.positions().stream().flatMap(position -> {
            List<int[]> surroundingFieldsPositions = getSurroundingFieldsPositions(position.row(), position.col());
            Stream<Position> surroundingPositionsStream = surroundingFieldsPositions.stream().map(positon -> ImmutablePosition.builder().row(positon[0]).col(positon[1]).build());
            return surroundingPositionsStream;
        }).collect(Collectors.toSet());
        java.util.Map<Field, Long> surroundingFieldCounts = allSurroundingFieldsPositions.stream().map(position -> fields[position.row()][position.col()]).collect(Collectors.groupingBy(p -> p, Collectors.counting()));
        surroundingFieldCounts.remove(fieldCluster.field());
        return surroundingFieldCounts;
    }

    public boolean hasSurroundingBorder(FieldCluster fieldCluster) {
        long fieldsWithSurroundingBorderCount = fieldCluster.positions().stream().filter(position -> {
            int row = position.row();
            int col = position.col();
            if (row == 0 || col == 0)
                return true;
            if (row == rows() - 1 || col == cols() - 1)
                return true;
            return false;
        }).count();
        boolean hasSurroundingBorder = fieldsWithSurroundingBorderCount > 0;
        return hasSurroundingBorder;
    }

    @JsonIgnore
    public Field[] getBorderFields() {
        int rows = fields.length;
        int cols = fields[0].length;
        Field[] fieldsBorder = new Field[2 * cols + 2 * (rows - 2)];
        int ind = 0;
        for (Field field : fields[0])
            fieldsBorder[ind++] = field;
        for (Field field : fields[rows - 1])
            fieldsBorder[ind++] = field;
        for (int row = 1; row < rows - 1; row++) {
            fieldsBorder[ind++] = fields[row][0];
            fieldsBorder[ind++] = fields[row][cols - 1];
        }
        return fieldsBorder;
    }

    public List<FieldCluster> findFieldClusters(Field clusterField) {
        Field[][] fields = fields();
        Set<Position> positionsInCluster = new HashSet<>();
        List<FieldCluster> fieldClusters = new ArrayList<>();
        for (int row = 0; row < rows(); row++) {
            for (int col = 0; col < cols(); col++) {
                Field field = fields[row][col];
                Position position = ImmutablePosition.builder().row(row).col(col).build();
                if (clusterField == null && (field == Field.Empty || field == Field.Ruin || field == Field.Mountain) || clusterField != null && field != clusterField) {
                    continue;
                }
                boolean fieldAlreadyInFoundCluster = positionsInCluster.contains(position);
                if (fieldAlreadyInFoundCluster) {
                    continue;
                }
                FieldCluster fieldCluster = getClusterAtPosition(position);
                if (fieldCluster != null) {
                    fieldClusters.add(fieldCluster);
                    positionsInCluster.addAll(fieldCluster.positions());
                }
            }
        }
        return fieldClusters;
    }

    public FieldCluster getClusterAtPosition(Position position) {
        Set<Position> positionsVisited = new HashSet<>();
        Field fieldCluster = fields[position.row()][position.col()];
        expandClusterAtPosition(position.row(), position.col(), positionsVisited, fieldCluster);
        List<Position> positionsCluster = positionsVisited.stream().filter(positionVisited -> {
            Field fieldVisited = fields[positionVisited.row()][positionVisited.col()];
            return fieldVisited == fieldCluster;
        }).collect(Collectors.toList());
        FieldCluster fieldClusterResult = ImmutableFieldCluster.builder().position(position).field(fieldCluster).positions(positionsCluster).build();
        return fieldClusterResult;
    }

    private void expandClusterAtPosition(int row, int col, Set<Position> positionsVisited, Field fieldCluster) {
        Field field = fields[row][col];
        Position position = ImmutablePosition.builder().row(row).col(col).build();
        if (positionsVisited.contains(position))
            return;
        positionsVisited.add(position);
        if (field != fieldCluster)
            return;
        if (row > 0)
            expandClusterAtPosition(row - 1, col, positionsVisited, fieldCluster);
        if (col > 0)
            expandClusterAtPosition(row, col - 1, positionsVisited, fieldCluster);
        if (row < rows() - 1)
            expandClusterAtPosition(row + 1, col, positionsVisited, fieldCluster);
        if (col < cols() - 1)
            expandClusterAtPosition(row, col + 1, positionsVisited, fieldCluster);
    }

    @Value.Immutable
    public static abstract class FieldCluster {
        public abstract Position position();

        public abstract Field field();

        public abstract List<Position> positions();
    }
}
