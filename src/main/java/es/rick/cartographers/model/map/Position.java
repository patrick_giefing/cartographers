package es.rick.cartographers.model.map;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutablePosition.class)
@JsonDeserialize(as = ImmutablePosition.class)
public abstract class Position {
    public abstract int row();

    public abstract int col();

    public int[] toArray() {
        return new int[]{row(), col()};
    }

    @Override
    public int hashCode() {
        return row() * 100_000_000 + col();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }
}
