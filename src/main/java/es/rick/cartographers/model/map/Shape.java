package es.rick.cartographers.model.map;

import org.immutables.value.Value;

import java.util.HashSet;
import java.util.Set;

@Value.Immutable
public abstract class Shape {
    public abstract boolean[][] shape();

    public Shape[] getVariations() {
        Shape shapeRotated = this;
        Set<Shape> variations = new HashSet<>();
        for (int rotation = 0; rotation < 2; rotation++) {
            variations.add(shapeRotated);
            variations.add(shapeRotated.flipHorizontally());
            variations.add(shapeRotated.flipVertically());
            variations.add(shapeRotated.flipHorizontally().flipVertically());
            shapeRotated = shapeRotated.rotateRight();
        }
        return variations.toArray(new Shape[0]);
    }

    @Override
    public boolean equals(Object obj) {
        return this.toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    public Shape flipHorizontally() {
        boolean[][] shape = shape();
        boolean[][] shapeMH = new boolean[shape.length][];
        for (int i = 0; i < shape.length; i++) {
            boolean[] line = shape[i];
            boolean[] lineMH = new boolean[line.length];
            for (int j = 0; j < line.length; j++) {
                lineMH[line.length - j - 1] = line[j];
            }
            shapeMH[i] = lineMH;
        }
        return ImmutableShape.builder().shape(shapeMH).build();
    }

    public Shape flipVertically() {
        boolean[][] shape = shape();
        boolean[][] shapeVH = new boolean[shape.length][];
        for (int i = 0; i < shape.length; i++) {
            boolean[] line = shape[shape.length - i - 1];
            shapeVH[i] = line;
        }
        return ImmutableShape.builder().shape(shapeVH).build();
    }

    public Shape rotateRight() {
        boolean[][] shape = shape();
        int rowsR = shape[0].length;
        int colsR = shape.length;
        boolean[][] shapeR = new boolean[rowsR][];
        for (int i = 0; i < rowsR; i++) {
            boolean[] rowR = new boolean[colsR];
            for (int j = 0; j < colsR; j++) {
                rowR[j] = shape[j][i];
            }
            shapeR[i] = rowR;
        }
        return ImmutableShape.builder().shape(shapeR).build();
    }

    @Override
    public String toString() {
        boolean shape[][] = shape();
        int rows = shape.length;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            if (i > 0)
                sb.append(System.lineSeparator());
            boolean[] row = shape[i];
            for (int j = 0; j < row.length; j++) {
                sb.append(row[j] ? "*" : " ");
            }
        }
        return sb.toString();
    }
}
