package es.rick.cartographers.model.map;

public enum Field {
    Empty, Ruin, Mountain, Rift,
    Forest, Village, Farm, Water, Monster
}
