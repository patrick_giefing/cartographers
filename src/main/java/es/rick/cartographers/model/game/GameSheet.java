package es.rick.cartographers.model.game;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import es.rick.cartographers.model.map.Map;
import es.rick.cartographers.model.player.Player;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableGameSheet.class)
@JsonDeserialize(as = ImmutableGameSheet.class)
public abstract class GameSheet {
    public abstract Map map();

    public abstract Player player();
}
