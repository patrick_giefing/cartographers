package es.rick.cartographers.model.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import es.rick.cartographers.model.card.ExploreCardDrawer;
import es.rick.cartographers.model.card.SeasonScoringCard;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@JsonSerialize(as = ImmutableGame.class)
@JsonDeserialize(as = ImmutableGame.class)
public abstract class Game {
    public abstract List<SeasonScoringCard> seasonScoringCards();

    public abstract List<GameSheet> gameSheets();

    @JsonIgnore
    public abstract ExploreCardDrawer exploreCardDrawer();
}
