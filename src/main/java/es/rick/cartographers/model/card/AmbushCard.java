package es.rick.cartographers.model.card;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import es.rick.cartographers.model.enums.Direction;
import es.rick.cartographers.model.map.Shape;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableAmbushCard.class)
@JsonDeserialize(as = ImmutableAmbushCard.class)
public abstract class AmbushCard extends ExploreCard {
    public abstract Direction direction();

    public abstract Shape shape();
}
