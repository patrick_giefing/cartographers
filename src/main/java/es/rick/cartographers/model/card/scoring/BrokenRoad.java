package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

public class BrokenRoad extends ScoringCard {
    @Override
    public String name() {
        return "The Broken Road";
    }

    @Override
    public String description() {
        return "Earn three reputation stars for each complete diagonal line of filled spaces that touches the left and bottom edges of the map.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Path;
    }

    @Override
    public int calculatePoints(Map map) {
        Field[][] fields = map.fields();
        int points = 0;
        rows:
        for (int row = 0; row < map.rows(); row++) {
            int minLength = Math.min(map.rows(), map.cols());
            for (int ind = 0; ind < minLength - row; ind++) {
                Field field = fields[row + ind][ind];
                if (field == Field.Empty || field == Field.Ruin)
                    continue rows;
            }
            points += 3;
        }
        return points;
    }
}
