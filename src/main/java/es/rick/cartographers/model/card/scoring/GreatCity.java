package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

import static es.rick.cartographers.model.map.Field.*;

public class GreatCity extends ScoringCard {
    @Override
    public String name() {
        return "Great City";
    }

    @Override
    public String description() {
        return "Earn one reputation star for each village space in the largest cluster of village spaces that is not adjacent to a mountain space.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Village;
    }

    @Override
    public int calculatePoints(Map map) {
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(Village);
        int points = fieldClusters.stream().map(fieldCluster -> {
            java.util.Map<Field, Long> surroundingFieldCounts = map.getSurroundingFieldCounts(fieldCluster);
            long mountainCount = surroundingFieldCounts.getOrDefault(Mountain, 0l);
            if (mountainCount > 0)
                return 0;
            int villageCount = fieldCluster.positions().size();
            return villageCount;
        }).max((a, b) -> a.compareTo(b)).orElse(0);
        return (int) points;
    }
}
