package es.rick.cartographers.model.card;

import es.rick.cartographers.model.card.factory.AmbushCardFactory;
import es.rick.cartographers.model.card.factory.TerrainCardFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ExploreCardDrawer {
    private final List<AmbushCard> ambushCards;
    private final List<ExploreCard> exploreCards;
    private boolean shuffled = false;

    private ExploreCardDrawer(Collection<TerrainCard> terrainCards, Collection<AmbushCard> ambushCards, Collection<RuinsCard> ruinsCards) {
        this.exploreCards = new ArrayList<>(terrainCards);
        this.exploreCards.addAll(ruinsCards);
        this.ambushCards = new ArrayList<>(ambushCards);
    }

    public void addRandomAmbushCardAndShuffle() {
        Collections.shuffle(this.ambushCards);
        if (this.ambushCards.isEmpty())
            throw new IllegalArgumentException("No ambush card left. There should be at least as many ambush cards as there are seasons.");
        AmbushCard ambushCard = this.ambushCards.remove(0);
        this.exploreCards.add(ambushCard);
        Collections.shuffle(this.exploreCards);
    }

    public static ExploreCardDrawer createRandomExploreCardDrawer() {
        List<TerrainCard> terrainCards = TerrainCardFactory.createTerrainCards();
        List<AmbushCard> ambushCards = AmbushCardFactory.createAmbushCards();
        return new ExploreCardDrawer(terrainCards, ambushCards, List.of(new RuinsCard(), new RuinsCard()));
    }

    public DrawnExploreCard drawExploreCard() {
        if (!shuffled)
            throw new IllegalArgumentException("You need to shuffle the deck at least once before you draw a card.");
        if (exploreCards.isEmpty())
            throw new IllegalArgumentException("No cards left in the deck.");
        ExploreCard exploreCard = null;
        boolean mustPlaceOnRuin = false;
        while (!(exploreCard instanceof TerrainCard)) {
            exploreCard = exploreCards.remove(0);
            if (exploreCard instanceof RuinsCard)
                mustPlaceOnRuin = true;
        }
        ImmutableDrawnExploreCard drawnExploreCard = ImmutableDrawnExploreCard.builder().exploreCard(exploreCard).mustPlaceOnRuin(mustPlaceOnRuin).build();
        return drawnExploreCard;
    }
}
