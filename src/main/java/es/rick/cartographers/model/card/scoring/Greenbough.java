package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

public class Greenbough extends ScoringCard {
    @Override
    public String name() {
        return "Greenbough:";
    }

    @Override
    public String description() {
        return "Earn one reputation star for each row and column with at least one forest space. The same forest space may be scored in a row and a column.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Forest;
    }

    @Override
    public int calculatePoints(Map map) {
        Field[][] fields = map.fields();
        int points = 0;
        for (int row = 0; row < map.rows(); row++) {
            for (int col = 0; col < map.cols(); col++) {
                if (fields[row][col] == Field.Forest) {
                    points++;
                    break;
                }
            }
        }
        for (int col = 0; col < map.cols(); col++) {
            for (int row = 0; row < map.rows(); row++) {
                if (fields[row][col] == Field.Forest) {
                    points++;
                    break;
                }
            }
        }
        return points;
    }
}
