package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

public class GoldenGranary extends ScoringCard {
    @Override
    public String name() {
        return "The Golden Granary";
    }

    @Override
    public String description() {
        return "Earn one reputation star for each water space adjacent to a ruins space. Earn three reputation stars for each farm space on a ruins space.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.FarmWater;
    }

    @Override
    public int calculatePoints(Map map) {
        int[][] ruinsPositions = map.getRuinsPositions();
        int points = 0;
        Field[][] fields = map.fields();
        for (int[] ruinPositions : ruinsPositions) {
            if (fields[ruinPositions[0]][ruinPositions[1]] == Field.Farm)
                points += 3;
            Field[] surroundingFields = map.getSurroundingFields(ruinPositions[0], ruinPositions[1]);
            points += List.of(surroundingFields).stream().filter(f -> f == Field.Water).count();
        }
        return points;
    }
}
