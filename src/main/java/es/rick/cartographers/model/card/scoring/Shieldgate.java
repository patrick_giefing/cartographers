package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Map;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static es.rick.cartographers.model.map.Field.Village;

public class Shieldgate extends ScoringCard {
    @Override
    public String name() {
        return "Shieldgate:";
    }

    @Override
    public String description() {
        return "Earn two reputation stars for each village space in the second largest cluster of village spaces.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Village;
    }

    @Override
    public int calculatePoints(Map map) {
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(Village);
        java.util.Map<Integer, Long> clusterCounts = new TreeMap<>(fieldClusters.stream().collect(Collectors.groupingBy(it -> it.positions().size(), Collectors.counting())));
        if (clusterCounts.size() < 2) {
            return 0;
        }
        ArrayList<Integer> villageSizes = new ArrayList<>(clusterCounts.keySet());
        int villageSize2ndHighest = villageSizes.get(villageSizes.size() - 2);
        long villageSize2ndHighestCount = clusterCounts.get(villageSize2ndHighest);
        int points = villageSize2ndHighest * (int) villageSize2ndHighestCount * 2;
        return points;
    }
}
