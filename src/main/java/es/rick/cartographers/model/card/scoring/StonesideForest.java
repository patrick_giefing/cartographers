package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;
import java.util.stream.IntStream;

import static es.rick.cartographers.model.map.Field.Forest;
import static es.rick.cartographers.model.map.Field.Mountain;

public class StonesideForest extends ScoringCard {
    @Override
    public String name() {
        return "Stoneside Forest";
    }

    @Override
    public String description() {
        return "Earn three reputation stars for each mountain space connected to another mountain space by a cluster of forest spaces.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Forest;
    }

    @Override
    public int calculatePoints(Map map) {
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(Forest);
        int points = fieldClusters.stream().map(fieldCluster -> {
            java.util.Map<Field, Long> surroundingFieldCounts = map.getSurroundingFieldCounts(fieldCluster);
            long count = surroundingFieldCounts.getOrDefault(Mountain, 0l);
            if (count < 2)
                return 0;
            int clusterPoints = IntStream.rangeClosed(0, (int) count - 1).reduce(0, (int x, int y) -> x + y) * 3;
            return clusterPoints;
        }).reduce((a, b) -> a + b).orElse(0);
        return points;
    }
}
