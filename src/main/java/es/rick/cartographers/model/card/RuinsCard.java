package es.rick.cartographers.model.card;

public final class RuinsCard extends ExploreCard {
    private static final RuinsCard INSTANCE = new RuinsCard();

    public static RuinsCard getInstance() {
        return INSTANCE;
    }
}
