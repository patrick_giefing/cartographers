package es.rick.cartographers.model.card.factory;

import es.rick.cartographers.model.card.AmbushCard;
import es.rick.cartographers.model.card.ImmutableAmbushCard;
import es.rick.cartographers.model.enums.Direction;
import es.rick.cartographers.model.map.ImmutableShape;

import java.util.ArrayList;
import java.util.List;

public final class AmbushCardFactory {
    public static List<AmbushCard> createAmbushCards() {
        List<AmbushCard> ambushCards = new ArrayList<>();
        {
            boolean[][] shape = {
                    {true, false, false},
                    {false, true, false},
                    {false, false, true},
            };
            ambushCards.add(ImmutableAmbushCard.builder().shape(ImmutableShape.builder().shape(shape).build()).direction(Direction.Counterclockwise).build());
        }
        {
            boolean[][] shape = {
                    {true, false, true},
                    {true, false, true}
            };
            ambushCards.add(ImmutableAmbushCard.builder().shape(ImmutableShape.builder().shape(shape).build()).direction(Direction.Clockwise).build());
        }
        {
            boolean[][] shape = {
                    {true, false},
                    {true, true},
                    {true, false}
            };
            ambushCards.add(ImmutableAmbushCard.builder().shape(ImmutableShape.builder().shape(shape).build()).direction(Direction.Clockwise).build());
        }
        {
            boolean[][] shape = {
                    {true, true},
                    {true, false},
                    {true, true}
            };
            ambushCards.add(ImmutableAmbushCard.builder().shape(ImmutableShape.builder().shape(shape).build()).direction(Direction.Counterclockwise).build());
        }
        return ambushCards;
    }
}
