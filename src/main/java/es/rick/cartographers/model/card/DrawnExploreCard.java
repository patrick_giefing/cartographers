package es.rick.cartographers.model.card;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableDrawnExploreCard.class)
@JsonDeserialize(as = ImmutableDrawnExploreCard.class)
public abstract class DrawnExploreCard {
    public abstract ExploreCard exploreCard();

    public abstract boolean mustPlaceOnRuin();
}
