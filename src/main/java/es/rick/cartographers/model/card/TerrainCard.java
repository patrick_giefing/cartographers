package es.rick.cartographers.model.card;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import es.rick.cartographers.model.enums.Terrain;
import es.rick.cartographers.model.map.Shape;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@JsonSerialize(as = ImmutableTerrainCard.class)
@JsonDeserialize(as = ImmutableTerrainCard.class)
public abstract class TerrainCard extends ExploreCard {
    public abstract List<Terrain> possibleTerrains();

    public abstract List<ShapeOption> possibleShapes();

    public abstract int points();

    @Value.Immutable
    @JsonSerialize(as = ImmutableShapeOption.class)
    @JsonDeserialize(as = ImmutableShapeOption.class)
    public static abstract class ShapeOption {
        public abstract Shape shape();

        public abstract int gold();
    }
}
