package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

import static es.rick.cartographers.model.map.Field.Farm;
import static es.rick.cartographers.model.map.Field.Water;

public class ShoresideExpanse extends ScoringCard {
    @Override
    public String name() {
        return "Shoreside Expanse";
    }

    @Override
    public String description() {
        return "Earn three reputation stars for each cluster of farm spaces not adjacent to a water space or the edge of the map. Earn three reputation stars for each cluster of water spaces not adjacent to a farm space or the edge of the map.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.FarmWater;
    }

    @Override
    public int calculatePoints(Map map) {
        int pointsFarmClusters = getPoints(map, Farm, Water);
        int pointsWaterClusters = getPoints(map, Water, Farm);
        int points = pointsFarmClusters + pointsWaterClusters;
        return points;
    }

    private static int getPoints(Map map, Field fieldA, Field fieldB) {
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(fieldA);
        long fieldClusterCountWithoutFieldNeighbourAndBorder = fieldClusters.stream().filter(fieldCluster -> {
            java.util.Map<Field, Long> surroundingFieldCounts = map.getSurroundingFieldCounts(fieldCluster);
            long fieldNeighbourCount = surroundingFieldCounts.getOrDefault(fieldB, 0l);
            if (fieldNeighbourCount > 0)
                return false;
            boolean hasSurroundingBorder = map.hasSurroundingBorder(fieldCluster);
            if (hasSurroundingBorder)
                return false;
            return true;
        }).count();
        return (int) fieldClusterCountWithoutFieldNeighbourAndBorder * 3;
    }
}
