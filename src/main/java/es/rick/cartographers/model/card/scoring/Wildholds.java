package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Map;

import java.util.List;

import static es.rick.cartographers.model.map.Field.Village;

public class Wildholds extends ScoringCard {
    @Override
    public String name() {
        return "Wildholds";
    }

    @Override
    public String description() {
        return "Earn eight reputation stars for each cluster of six or more village spaces.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Village;
    }

    @Override
    public int calculatePoints(Map map) {
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(Village);
        long points = fieldClusters.stream().filter(fieldCluster -> fieldCluster.positions().size() >= 6).count() * 8;
        return (int) points;
    }
}
