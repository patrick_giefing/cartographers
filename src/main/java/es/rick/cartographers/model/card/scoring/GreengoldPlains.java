package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

import static es.rick.cartographers.model.map.Field.*;

public class GreengoldPlains extends ScoringCard {
    @Override
    public String name() {
        return "Greengold Plains";
    }

    @Override
    public String description() {
        return "Earn three reputation stars for each cluster of villages spaces that is adjacent to three or more different terrain types.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Village;
    }

    @Override
    public int calculatePoints(Map map) {
        List<Map.FieldCluster> fieldClusters = map.findFieldClusters(Village);
        long points = fieldClusters.stream().filter(fieldCluster -> {
            java.util.Map<Field, Long> surroundingFieldCounts = map.getSurroundingFieldCounts(fieldCluster);
            surroundingFieldCounts.remove(Mountain);
            surroundingFieldCounts.remove(Ruin);
            return surroundingFieldCounts.size() >= 3;
        }).count() * 3;
        return (int) points;
    }
}
