package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

public class LostBarony extends ScoringCard {
    @Override
    public String name() {
        return "Lost Barony";
    }

    @Override
    public String description() {
        return "Earn three reputation stars for each space along one edge of the largest square of filled spaces.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Path;
    }

    @Override
    public int calculatePoints(Map map) {
        int biggestSquareSize = 0;
        int biggestSquareCount = 0;
        for (int row = 0; row < map.rows(); row++) {
            for (int col = 0; col < map.cols(); col++) {
                int squareSizeAtPosition = getSquareSizeAtPosition(row, col, map, biggestSquareSize);
                if (squareSizeAtPosition > biggestSquareSize) {
                    biggestSquareSize = squareSizeAtPosition;
                    biggestSquareCount = 1;
                } else if (squareSizeAtPosition == biggestSquareSize) {
                    biggestSquareCount++;
                }
            }
        }
        int points = biggestSquareSize * biggestSquareCount;
        return points;
    }

    private static int getSquareSizeAtPosition(int row, int col, Map map, int biggestSquareSize) {
        Field[][] fields = map.fields();
        int rows = map.rows();
        int cols = map.cols();
        int maxPossibleSquareSize = Math.min(rows - row - 1, cols - col - 1);
        if (maxPossibleSquareSize < biggestSquareSize) {
            return -1;
        }
        int biggestSquareSizeFound = 0;
        for (int squareSize = Math.min(biggestSquareSize, 1); squareSize <= maxPossibleSquareSize; squareSize++) {
            for (int squareRow = row; squareRow < row + maxPossibleSquareSize; squareRow++) {
                for (int squareCol = col; squareCol < col + maxPossibleSquareSize; squareCol++) {
                    Field field = fields[squareRow][squareCol];
                    if (field == Field.Empty || field == Field.Ruin) {
                        return biggestSquareSizeFound;
                    }
                }
            }
            biggestSquareSizeFound = squareSize;
        }
        return biggestSquareSizeFound;
    }
}
