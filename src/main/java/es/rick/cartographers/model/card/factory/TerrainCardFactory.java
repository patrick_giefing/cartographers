package es.rick.cartographers.model.card.factory;

import es.rick.cartographers.model.card.ImmutableShapeOption;
import es.rick.cartographers.model.card.ImmutableTerrainCard;
import es.rick.cartographers.model.card.TerrainCard;
import es.rick.cartographers.model.enums.Terrain;
import es.rick.cartographers.model.map.ImmutableShape;

import java.util.List;

public final class TerrainCardFactory {
    public static List<TerrainCard> createTerrainCards() {
        List<TerrainCard> terrainCards = List.of(
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Water)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true},
                                                        {true},
                                                        {true}
                                                }).build())
                                        .gold(1).build(),
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {false, false, true},
                                                        {false, true, true},
                                                        {true, true, false}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(1).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Farm)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true},
                                                        {true}
                                                }).build())
                                        .gold(1).build(),
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {false, true, false},
                                                        {true, true, true},
                                                        {false, true, false}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(1).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Village)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, false},
                                                        {true, true}
                                                }).build())
                                        .gold(1).build(),
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, true, true},
                                                        {true, true, false}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(1).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Forest)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, false},
                                                        {false, true}
                                                }).build())
                                        .gold(1).build(),
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, false},
                                                        {true, true},
                                                        {false, true}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(1).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Farm, Terrain.Water)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, true, true},
                                                        {true, false, false},
                                                        {true, false, false}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(2).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Village, Terrain.Farm)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, false},
                                                        {true, true},
                                                        {true, false}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(2).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Forest, Terrain.Farm)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, true, true},
                                                        {false, false, true}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(2).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Forest, Terrain.Village)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {false, false, true, true},
                                                        {true, true, true, false}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(2).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Forest, Terrain.Water)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, false, false},
                                                        {true, true, true},
                                                        {true, false, false}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(2).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Village, Terrain.Water)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true, true, true, true}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(2).build(),
                ImmutableTerrainCard.builder().possibleTerrains(List.of(Terrain.Forest, Terrain.Village, Terrain.Farm, Terrain.Water, Terrain.Monster)).possibleShapes(
                        List.of(
                                ImmutableShapeOption.builder()
                                        .shape(ImmutableShape.builder()
                                                .shape(new boolean[][]{
                                                        {true}
                                                }).build())
                                        .gold(0).build()
                        )
                ).points(0).build()
        );
        return terrainCards;
    }
}
