package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

import static es.rick.cartographers.model.map.Field.*;

public class Cauldrons extends ScoringCard {
    @Override
    public String name() {
        return "The Cauldrons:";
    }

    @Override
    public String description() {
        return "Earn one reputation star for each empty space surrounded on all four sides by filled spaces or the edge of the map.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Path;
    }

    @Override
    public int calculatePoints(Map map) {
        Field[][] fields = map.fields();
        int points = 0;
        for (int row = 0; row < map.rows(); row++) {
            for (int col = 0; col < map.cols(); col++) {
                if (fields[row][col] != Empty && fields[row][col] != Ruin)
                    continue;
                Field[] fieldsSurrounding = map.getSurroundingFields(row, col);
                int fieldsSurroundingNotEmptyCount = (int) List.of(fieldsSurrounding).stream().filter(f -> f != Empty && f != Ruin).count();
                if (fieldsSurrounding.length == fieldsSurroundingNotEmptyCount)
                    points++;
            }
        }
        return points;
    }
}
