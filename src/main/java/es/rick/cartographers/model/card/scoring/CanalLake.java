package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

public class CanalLake extends ScoringCard {
    @Override
    public String name() {
        return "Canal Lake";
    }

    @Override
    public String description() {
        return "Earn one reputation star for each water space adjacent to at least one farm space. Earn one reputation star for each farm space adjacent to at least one water space.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.FarmWater;
    }

    @Override
    public int calculatePoints(Map map) {
        Field[][] fields = map.fields();
        int points = 0;
        for (int row = 0; row < map.rows(); row++) {
            for (int col = 0; col < map.cols(); col++) {
                if (fields[row][col] == Field.Water) {
                    Field[] fieldsSurrounding = map.getSurroundingFields(row, col);
                    int fieldsSurroundingFarmCount = (int) List.of(fieldsSurrounding).stream().filter(f -> f == Field.Farm).count();
                    if (fieldsSurroundingFarmCount > 0)
                        points++;
                }
                if (fields[row][col] == Field.Farm) {
                    Field[] fieldsSurrounding = map.getSurroundingFields(row, col);
                    int fieldsSurroundingFarmCount = (int) List.of(fieldsSurrounding).stream().filter(f -> f == Field.Water).count();
                    if (fieldsSurroundingFarmCount > 0)
                        points++;
                }
            }
        }
        return points;
    }
}
