package es.rick.cartographers.model.card;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import es.rick.cartographers.model.card.scoring.*;
import es.rick.cartographers.model.enums.Season;
import es.rick.cartographers.model.map.Map;
import kotlin.NotImplementedError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "clazz")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Borderlands.class, name = "Borderlands"),
        @JsonSubTypes.Type(value = BrokenRoad.class, name = "BrokenRoad"),
        @JsonSubTypes.Type(value = CanalLake.class, name = "CanalLake"),
        @JsonSubTypes.Type(value = Cauldrons.class, name = "Cauldrons"),
        @JsonSubTypes.Type(value = GoldenGranary.class, name = "GoldenGranary"),
        @JsonSubTypes.Type(value = GreatCity.class, name = "GreatCity"),
        @JsonSubTypes.Type(value = Greenbough.class, name = "Greenbough"),
        @JsonSubTypes.Type(value = GreengoldPlains.class, name = "GreengoldPlains"),
        @JsonSubTypes.Type(value = LostBarony.class, name = "LostBarony"),
        @JsonSubTypes.Type(value = MagesValley.class, name = "MagesValley"),
        @JsonSubTypes.Type(value = SentinelWood.class, name = "SentinelWood"),
        @JsonSubTypes.Type(value = Shieldgate.class, name = "Shieldgate"),
        @JsonSubTypes.Type(value = ShoresideExpanse.class, name = "ShoresideExpanse"),
        @JsonSubTypes.Type(value = StonesideForest.class, name = "StonesideForest"),
        @JsonSubTypes.Type(value = Treetower.class, name = "Treetower"),
        @JsonSubTypes.Type(value = Wildholds.class, name = "Wildholds")
})
public abstract class ScoringCard {
    @JsonProperty("name")
    public abstract String name();

    @JsonProperty("description")
    public abstract String description();

    @JsonProperty("group")
    public abstract ScoringCardGroup group();

    public int calculatePoints(Map map) {
        throw new NotImplementedError("not implemented");
    }

    public enum ScoringCardGroup {
        Forest, FarmWater, Path, Village
    }

    public static List<ScoringCard> getScoringCards() {
        return List.of(
                new Borderlands(), new BrokenRoad(), new CanalLake(), new Cauldrons(),
                new GoldenGranary(), new GreatCity(), new Greenbough(), new GreengoldPlains(),
                new LostBarony(), new MagesValley(), new SentinelWood(), new Shieldgate(),
                new ShoresideExpanse(), new StonesideForest(), new Treetower(), new Wildholds()
        );
    }

    public static java.util.Map<ScoringCardGroup, List<ScoringCard>> getGroupedScoringCards() {
        List<ScoringCard> scoringCards = getScoringCards();
        java.util.Map<ScoringCardGroup, List<ScoringCard>> groupedScoringCards = scoringCards.stream().collect(Collectors.groupingBy(it -> it.group()));
        return groupedScoringCards;
    }

    public static List<SeasonScoringCard> buildRandomSeasonScoringCards() {
        java.util.Map<ScoringCardGroup, List<ScoringCard>> groupedScoringCards = getGroupedScoringCards();
        if (groupedScoringCards.size() < Season.values().length) {
            throw new IllegalArgumentException("There should be at least as many scoring card groups as there are seasons.");
        }
        List<List<ScoringCard>> groupedScoringCardLists = new ArrayList<>(groupedScoringCards.values());
        Collections.shuffle(groupedScoringCardLists);
        List<ScoringCard> scoringCardsPickedRandom = IntStream.range(0, groupedScoringCardLists.size()).mapToObj(i -> {
            List<ScoringCard> scoringCards = groupedScoringCardLists.get(i);
            Collections.shuffle(scoringCards);
            return scoringCards.get(0);
        }).collect(Collectors.toList());
        List<SeasonScoringCard> seasonScoringCards = new ArrayList<>();
        for (int i = 0; i < Season.values().length; i++) {
            Season season = Season.values()[i];
            ScoringCard seasonCard1 = scoringCardsPickedRandom.get(i);
            ScoringCard seasonCard2 = scoringCardsPickedRandom.get(i < Season.values().length - 1 ? i + 1 : 0);
            seasonScoringCards.add(ImmutableSeasonScoringCard.builder().season(season).scoringCards(List.of(seasonCard1, seasonCard2)).build());
        }
        return seasonScoringCards;
    }
}
