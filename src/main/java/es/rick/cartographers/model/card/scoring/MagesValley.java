package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

public class MagesValley extends ScoringCard {
    @Override
    public String name() {
        return "Mages Valley";
    }

    @Override
    public String description() {
        return "Earn two reputation stars for each water space adjacent to a mountain space. Earn one reputation star for each farm space adjacent to a mountain space.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.FarmWater;
    }

    @Override
    public int calculatePoints(Map map) {
        int[][] moutainsPositions = map.getMountainsPositions();
        int points = 0;
        for (int[] mountainPositions : moutainsPositions) {
            Field[] surroundingFields = map.getSurroundingFields(mountainPositions[0], mountainPositions[1]);
            int pointsMountain = List.of(surroundingFields).stream().map(f -> {
                if (f == Field.Water)
                    return 2;
                if (f == Field.Farm)
                    return 1;
                return 0;
            }).reduce((a, b) -> a + b).orElse(0);
            points += pointsMountain;
        }
        return points;
    }
}
