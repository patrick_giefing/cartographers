package es.rick.cartographers.model.card;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import es.rick.cartographers.model.enums.Season;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@JsonSerialize(as = ImmutableSeasonScoringCard.class)
@JsonDeserialize(as = ImmutableSeasonScoringCard.class)
public abstract class SeasonScoringCard {
    public abstract Season season();

    public abstract List<ScoringCard> scoringCards();
}
