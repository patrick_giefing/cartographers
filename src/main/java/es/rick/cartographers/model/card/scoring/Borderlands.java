package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import static es.rick.cartographers.model.map.Field.Empty;
import static es.rick.cartographers.model.map.Field.Ruin;

public class Borderlands extends ScoringCard {
    @Override
    public String name() {
        return "Borderlands:";
    }

    @Override
    public String description() {
        return "Earn six reputation stars for each complete row or complete column of filled spaces.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Path;
    }

    @Override
    public int calculatePoints(Map map) {
        Field[][] fields = map.fields();
        int points = 0;
        rows:
        for (int row = 0; row < map.rows(); row++) {
            for (int col = 0; col < map.cols(); col++) {
                Field field = fields[row][col];
                if (field == Empty || field == Ruin) {
                    continue rows;
                }
            }
            points += 6;
        }
        cols:
        for (int col = 0; col < map.cols(); col++) {
            for (int row = 0; row < map.rows(); row++) {
                Field field = fields[row][col];
                if (field == Empty || field == Ruin) {
                    continue cols;
                }
            }
            points += 6;
        }
        return points;
    }
}
