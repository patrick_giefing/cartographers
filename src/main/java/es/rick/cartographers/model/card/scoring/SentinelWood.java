package es.rick.cartographers.model.card.scoring;

import es.rick.cartographers.model.card.ScoringCard;
import es.rick.cartographers.model.map.Field;
import es.rick.cartographers.model.map.Map;

import java.util.List;

public class SentinelWood extends ScoringCard {
    @Override
    public String name() {
        return "Sentinel Wood";
    }

    @Override
    public String description() {
        return "Earn one reputation star for each forest space adjacent to the edge of the map.";
    }

    @Override
    public ScoringCardGroup group() {
        return ScoringCardGroup.Forest;
    }

    @Override
    public int calculatePoints(Map map) {
        Field[] fieldsBorder = map.getBorderFields();
        long points = List.of(fieldsBorder).stream().filter(f -> f == Field.Forest).count();
        return (int) points;
    }
}
