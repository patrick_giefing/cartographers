package es.rick.cartographers.model.player;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.List.of;

@Value.Immutable
@JsonSerialize(as = ImmutablePlayer.class)
@JsonDeserialize(as = ImmutablePlayer.class)
public abstract class Player {
    public abstract String name();

    public abstract String title();

    public abstract String province();

    public abstract String emblem();

    public static List<Player> createRandomPlayers(int playerCount) {
        List<Player> possiblePlayers = new ArrayList<>(of(
                ImmutablePlayer.builder().name("Alex").title("Duke").province("Norfolk").emblem("").build(),
                ImmutablePlayer.builder().name("Alice").title("Countess").province("Humberside").emblem("").build(),
                ImmutablePlayer.builder().name("Bob").title("Baron").province("Dyfed").emblem("").build(),
                ImmutablePlayer.builder().name("Max").title("Knight").province("Essex").emblem("").build(),
                ImmutablePlayer.builder().name("Eve").title("Lady").province("Cambridgeshire").emblem("").build(),
                ImmutablePlayer.builder().name("Robert").title("Esquire").province("Oxfordshire").emblem("").build(),
                ImmutablePlayer.builder().name("Steve").title("Marquees").province("Greater Manchester").emblem("").build(),
                ImmutablePlayer.builder().name("Tom").title("Viscount").province("West Midlands").emblem("").build(),
                ImmutablePlayer.builder().name("Kate").title("Marchioness").province("North Yorkshire").emblem("").build(),
                ImmutablePlayer.builder().name("Sam").title("Countess").province("Durham").emblem("").build()
        ));
        if (playerCount < 1)
            throw new IllegalArgumentException("At least 1 player needed.");
        if (playerCount > possiblePlayers.size())
            throw new IllegalArgumentException("Too many players - there are only " + possiblePlayers.size() + " players available.");
        Collections.shuffle(possiblePlayers);
        List<Player> players = IntStream.range(0, playerCount).mapToObj(i -> possiblePlayers.remove(0)).collect(Collectors.toList());
        return players;
    }
}
