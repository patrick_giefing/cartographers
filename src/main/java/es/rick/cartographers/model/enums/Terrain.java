package es.rick.cartographers.model.enums;

public enum Terrain {
    Forest(true), Village(true), Farm(true), Water(true), Monster(false);

    private final boolean usableForTerrainCard;

    Terrain(boolean usableForTerrainCard) {
        this.usableForTerrainCard = usableForTerrainCard;
    }

    public boolean usableForTerrainCard() {
        return usableForTerrainCard;
    }
}
