package es.rick.cartographers.model.enums;

public enum Season {
    Spring, Summer, Fall, Winter
}
