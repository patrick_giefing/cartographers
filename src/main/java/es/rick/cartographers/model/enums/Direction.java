package es.rick.cartographers.model.enums;

public enum Direction {
    Clockwise, Counterclockwise
}
